An API module that allows a developer to put their default views into individual plugin files which makes it easier to manage them.

You will only need this module if you are a developer or another module uses it to store default view definitions.

The module allows a developer to put default views into individual files which makes them easier to manage in a source code control system.

To use the module from YOURMODULE you need to implement the following methods:

function YOURMODULE_ctools_plugin_api($owner, $api) {

//  print "owner [$owner] api [$api]\n";

  if ($owner == 'views_plugin_default') {
    return array('version' => 1);
  }
}


function YOURMODULE_ctools_plugin_directory($module, $plugin) {
  if ($module == 'views_plugin_default') {
    return 'plugins/' . $plugin;
  }
}

This means that you can then place files in the directory:

path_to_you_module/plugins/default_view

each file is a plugin which defines a default view for the views module.

The plugins can be simply created by exporting the definition of a view and adding the following line to the bottom of an export.

$plugin = array('view' => $view);

This should be placed in the directory in a file with a .inc extension.

You can see examples of this in the module views_plugin_defaults_example which 
provides a simple content example view as a demonstration of the approach.
